<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'attack_detector';
$app['version'] = '2.3.13';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('attack_detector_app_description');
$app['powered_by'] = [
    'packages' => [
        'fail2ban-server' => [
            'name' => 'Fail2ban',
        ],
    ],
];

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('attack_detector_app_name');
$app['category'] = lang('base_category_gateway');
$app['subcategory'] = lang('base_subcategory_intrusion_protection');

/////////////////////////////////////////////////////////////////////////////
// Controllers
/////////////////////////////////////////////////////////////////////////////

$app['controllers']['attack_detector']['title'] = $app['name'];
$app['controllers']['settings']['title'] = lang('base_settings');
$app['controllers']['rules']['title'] = lang('attack_detector_rules');
$app['controllers']['bans']['title'] = lang('attack_detector_bans');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['requires'] = array(
    'app-network',
);

$app['core_requires'] = array(
    'app-events-core',
    'app-network-core',
    'fail2ban-server',
    'ipset'
);

$app['core_directory_manifest'] = array(
    '/var/clearos/attack_detector' => array(),
    '/var/clearos/attack_detector/filters' => array(),
    '/var/clearos/attack_detector/state' => array(),
    '/var/clearos/attack_detector/run' => array(
        'mode' => '0700'
    ),
);

$app['core_file_manifest'] = array(
    'fail2ban.php'=> array('target' => '/var/clearos/base/daemon/fail2ban.php'),
    'app-attack-detector.sudoers' => array(
        'target' => '/etc/sudoers.d/app-attack-detector',
        'mode' => '0440',
    ),
    '90-attack-detector' => array(
        'target' => '/etc/clearos/firewall.d/90-attack-detector',
        'mode' => '0755',
    ),
    'attack_detector.conf'=> array(
        'target' => '/etc/clearos/attack_detector.conf',
        'config' => TRUE,
        'config_params' => 'noreplace',
    ),
    'network-configuration-event'=> array(
        'target' => '/var/clearos/events/network_configuration/attack_detector',
        'mode' => '0755'
    ),

);

$app['delete_dependency'] = array(
    'app-attack-detector-core',
    'fail2ban-server',
);
